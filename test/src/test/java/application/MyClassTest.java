package application;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyClassTest {
    private MyClass subj;

    @Before
    public void init() {
        subj = new MyClass();
    }

    @Test
    public void addNumbers() {
        int result = subj.addNumbers(4, 7);
        assertEquals(11, result);
    }

    @Test
    public void subtractNumbers() {
        int result = subj.subtractNumbers(7, 3);
        assertEquals(4, result);
    }
}