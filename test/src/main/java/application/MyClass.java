package application;

public class MyClass {

    public int addNumbers(int a, int b) {
        return a + b;
    }

    public int subtractNumbers(int a, int b) {
        return a - b;
    }

    public int multiplyNumbers(int a, int b) {
        return a * b;
    }

    public int divideNumbers(int a, int b) {
        return a / b;
    }

    public void someUncoverMethod() {
        int a = 5;
        int b = 7;
        int c = a + b;
    }

    public void someUncoverMethodTwo() {
        int a = 5;
        int b = 7;
        int c = a + b;
    }

}
